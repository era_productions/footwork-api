<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*$factory->define(App\User::class, function (Faker\Generator $faker) {
  return [
      'name' => $faker->name,
      'email' => $faker->safeEmail,
      'password' => bcrypt(str_random(10)),
      'remember_token' => str_random(10),
  ];
});*/

$factory->define(App\Api\V1\Models\Event::class, function (Faker\Generator $faker) {
  $city = $faker->city;
  return [
      'name' => $city,
      'date' => $faker->date(),
      'description' => $faker->realText(),
      'duration' => $faker->numberBetween(0, 3) * 24 * 60 * 60 * 1000,
      'location' => $city,
      'lat' => $faker->randomFloat(6, -90, 90),
      'lng' => $faker->randomFloat(6, -180, 180),
      'iso3' => $faker->countryISOAlpha3
  ];
});

$factory->define(App\Api\V1\Models\Location::class, function(Faker\Generator $faker) {
  return [
    'lat' => $faker->randomFloat(6, -90.0, 90.0),
    'lng' => $faker->randomFloat(6, -180.0, 180.0)
  ];
});

$factory->define(App\Api\V1\Models\Path::class, function(Faker\Generator $faker) {
  return [
      'start_location_id' => $faker->randomNumber(2),
      'end_location_id' => $faker->randomNumber(2)
  ];
});
