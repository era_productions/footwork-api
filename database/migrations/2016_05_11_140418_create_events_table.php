<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('events', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 255);
      $table->date('date');
      $table->string('description', 50000);
      $table->integer('duration', false, true);
      $table->string('location', 1000);
      $table->float('lat', 8, 6);
      $table->float('lng', 9, 6);
      $table->string('iso3', 3);

      $table->index('date');

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('events');
  }
}
