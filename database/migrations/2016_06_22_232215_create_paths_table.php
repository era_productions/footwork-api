<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('paths', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('end_location_id', false, true);
      $table->integer('start_location_id', false, true);

      $table->foreign('end_location_id')->references('id')->on('locations');
      $table->foreign('start_location_id')->references('id')->on('locations');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('paths');
  }
}
