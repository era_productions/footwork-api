<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    factory(App\Api\V1\Models\Location::class, 100)->create();
  }
}
