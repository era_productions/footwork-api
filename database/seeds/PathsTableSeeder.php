<?php

use Illuminate\Database\Seeder;

class PathsTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    factory(App\Api\V1\Models\Path::class, 10)->create();
  }
}
