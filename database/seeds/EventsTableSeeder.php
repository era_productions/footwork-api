<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    factory(App\Api\V1\Models\Event::class, 200)->create();
  }
}
