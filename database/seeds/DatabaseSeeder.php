<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $this->call(EventsTableSeeder::class);
    $this->call(LocationsTableSeeder::class);
    $this->call(PathsTableSeeder::class);
    $this->call(UsersTableSeeder::class);
  }
}
