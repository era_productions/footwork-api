<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

/**
 * @param $api \Dingo\Api\Routing\Router
 * @param $routeName string
 */
function addRoutes($api, $routeName) {
  $api->get($routeName . '/rules', ['as' => $routeName . '.rules',
      'uses' => 'App\\Api\\V1\\Controllers\\' . ucfirst($routeName) . 'Controller@formRules']);
  $api->get($routeName . '/{id}',
      ['as' => $routeName . '.find', 'uses' => 'App\\Api\\V1\\Controllers\\' . ucfirst($routeName) . 'Controller@get']);
  $api->get($routeName . '/', ['as' => $routeName . '.all',
      'uses' => 'App\\Api\\V1\\Controllers\\' . ucfirst($routeName) . 'Controller@getAll']);
  /*$api->group(['middleware' => 'api.auth', 'providers' => 'jwt'],
      function (\Dingo\Api\Routing\Router $api) use ($routeName) {*/
  $api->delete($routeName . '/{id}', ['as' => $routeName . '.delete',
      'uses' => 'App\\Api\\V1\\Controllers\\' . ucfirst($routeName) . 'Controller@delete']);
  $api->post($routeName . '/', ['as' => $routeName . '.create',
      'uses' => 'App\\Api\\V1\\Controllers\\' . ucfirst($routeName) . 'Controller@create']);
  $api->put($routeName . '/', ['as' => $routeName . '.update',
      'uses' => 'App\\Api\\V1\\Controllers\\' . ucfirst($routeName) . 'Controller@update']);

  /*});*/
}

$api->version('v1', ['middleware' => 'api.throttle', 'limit' => '120', 'expires' => '2'], function ($api) {
  $api->group(['middleware' => 'api.auth', 'providers' => 'jwt'],
      function (\Dingo\Api\Routing\Router $api) use ($api) {
        $api->get('auth', 'App\Http\Controllers\AuthenticateController@authenticated');
      });
  $api->post('auth', 'App\Http\Controllers\AuthenticateController@authenticate');

  addRoutes($api, 'events');
  addRoutes($api, 'locations');
  addRoutes($api, 'paths');
});
