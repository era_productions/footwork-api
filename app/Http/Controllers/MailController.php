<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MailController extends Controller {
  use ValidatesRequests;

  public function mail(Request $request) {
    try {
      $this->validate($request, [
          'subject' => 'string|max:150',
          'body' => 'required|string|max:50000',
      ]);
    } catch(ValidationException $e) {
      throw new BadRequestHttpException($e->validator->errors());
    }
    Mail::send([], [], function($mail) use($request) {
      $mail->from('noreply@footwork.com', 'Footwork Noreplygit ');
      if(!$request->subject) {
        $subject = 'Info';
      } else {
        $subject = $request->subject;
      }
      $mail->to('info@footwork.com', 'Footwork Info')
          ->subject($subject)
          ->setBody($request->body);
    });
  }
}
