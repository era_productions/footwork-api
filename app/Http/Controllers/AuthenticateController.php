<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

use Tymon\JWTAuth\JWTAuth;

class AuthenticateController extends Controller {

  protected $auth;

  public function __construct(JWTAuth $auth) {
    $this->auth = $auth;
  }

  public function authenticate(Request $request) {
    $credentials = $request->json()->all();
    $validator = Validator::make($credentials, ['name' => 'string|required', 'password' => 'string|required']);
    if($validator->fails()) {
      return response()->json(['error' => 'bad_request'], 400);
    }
    try {
      // attempt to verify the credentials and create a token for the user
      if (! $token = $this->auth->attempt($credentials)) {
        return response()->json(['error' => 'invalid_credentials'], 401);
      }
    } catch (JWTException $e) {
      // something went wrong whilst attempting to encode the token
      return response()->json(['error' => 'could_not_create_token'], 500);
    }

    // all good so return the token
    return response()->json(compact('token'));
  }

  public function authenticated() {
    return app('Dingo\Api\Auth\Auth')->user();
  }
}
