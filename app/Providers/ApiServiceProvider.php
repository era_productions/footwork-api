<?php

namespace App\Providers;

use Dingo;
use Dingo\Api\Auth\Provider\JWT;
use League;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider {
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot() {
    app('Dingo\Api\Auth\Auth')->extend('jwt', function ($app) {
      return new JWT($app['Tymon\JWTAuth\JWTAuth']);
    });
    app('Dingo\Api\Exception\Handler')->setErrorFormat([
      'error' => [
        'message' => ':message',
        'errors' => ':errors',
        'code' => ':code',
        'status_code' => ':status_code',
        'debug' => ':debug'
      ]
    ]);
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register() {

  }
}
