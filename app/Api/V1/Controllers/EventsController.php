<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\Event;
use Illuminate\Http\Request;

class EventsController extends ApiController
{

  public function __construct()
  {
    parent::__construct(Event::class);
  }

  /**
   * @return array
   */
  public function rules()
  {
    $rules = [
      'name' => 'required|string|max:255',
      'date' => 'required',
      'description' => 'required|string|max:50000',
      'duration' => 'required|integer|min:0',
      'location' => 'required|string|max:1000',
      'lat' => 'required|numeric|max:90|min:-90',
      'lng' => 'required|numeric|max:180|min:-180',
      'iso3' => 'required|string|max:3'];
    return $rules;
  }
}