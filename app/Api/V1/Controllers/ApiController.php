<?php

namespace App\Api\V1\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class ApiController extends Controller {
  use Helpers;
  use ValidatesRequests;

  protected $modelClass;
  protected $primaryKeyName;

  protected function __construct($modelClass) {
    $this->modelClass = $modelClass;
    $model = new $modelClass();
    $this->primaryKeyName = $model->getKeyName();
  }

  public function create(Request $request) {
    $modelClass = $this->modelClass;
    $data = $request->json()->all();
    \Log::info($data);
    $validator = Validator::make($data, $this->rules());
    if ($validator->fails()) {
      throw new BadRequestHttpException($validator->errors());
    }
    /** @var Model $model */
    $model = new $modelClass();
    foreach ($model->getFillable() as $property) {
      $model->$property = $data[$property];
    }
    $model->save();
    return $model->toArray();
  }

  /**
   * @param $id mixed
   */
  public function delete($id) {
    $modelClass = $this->modelClass;
    $model = $modelClass::find($id);
    if (!$model) {
      return $this->response()->errorNotFound();
    }
    $model->delete();
    return $model->toArray();
  }

  /**
   * @param $id mixed
   * @return array
   */
  public function get($id) {
    $model = $this->find($id);
    if (!$model) {
      return $this->response()->errorNotFound();
    }
    return $model->toArray();
  }

  /**
   * @param Request $request
   * @return array
   */
  public function getAll(Request $request) {
    return $this->all($request)->toArray();
  }

  /**
   * @return array
   */
  protected abstract function rules();

  public function update(Request $request) {
    $modelClass = $this->modelClass;
    $primaryKeyName = $this->primaryKeyName;
    $data = $request->json()->all();
    $rules = array_merge([$primaryKeyName => 'required'], $this->rules());
    $validator = Validator::make($data, $rules);
    if ($validator->fails()) {
      throw new BadRequestHttpException($validator->errors());
    }
    $model = $modelClass::find($data[$primaryKeyName]);
    if (!$model) {
      return $this->response()->errorNotFound();
    }
    $count = 0;
    foreach ($model->getFillable() as $property) {
      if (array_key_exists($property, $data)) {
        $count++;
        $model->$property = $data[$property];
      }
    }
    if (0 != $count) {
      $model->save();
    }
    return $model->toArray();
  }

  /**
   * @param Request $request
   * @return mixed
   */
  protected function all(Request $request) {
    $modelClass = $this->modelClass;
    $orderBy = $request->order_by;
    $pageSize = $request->page_size;
    if($orderBy) {
      $direction = $request->direction;
      if(!$direction) {
        $direction = 'asc';
      }
      $items = $modelClass::orderBy($orderBy, $direction);
      if($pageSize) {
        return $items->paginate($pageSize);
      }
      return $items->get();
    }
    if($pageSize) {
      $paginator = $modelClass::paginate($pageSize);
      return $paginator;
    }
    return $modelClass::all();
  }

  /**
   * @param mixed $id
   * @return Model
   */
  protected function find($id) {
    $modelClass = $this->modelClass;
    return $modelClass::find($id);
  }
}