<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\Location;

class LocationsController extends ApiController {

  public function __construct() {
    parent::__construct(Location::class);
  }

  /**
   * @return array
   */
  public function formRules() {
    return [['name' => 'lat', 'max' => 90.0, 'min' => -90.0, 'precision' => 8, 'required' => true, 'scale' => '6',
        'type' => 'number'],
        ['name' => 'lng', 'max' => 180.0, 'min' => -180.0, 'precision' => 8, 'required' => true, 'scale' => '6',
            'type' => 'number']];
  }

  /**
   * @return array
   */
  public function rules() {
    return ['lat' => 'required|numeric|max:90.0|min:-90.0', 'lng' => 'required|numeric|max:180.0|min:-180.0'];
  }
}