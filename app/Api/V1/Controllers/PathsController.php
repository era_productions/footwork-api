<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\Path;

class PathsController extends ApiController {

  public function __construct() {
    parent::__construct(Path::class);
  }

  /**
   * @return array
   */
  public function formRules() {
    return [['name' => 'end_location_id', 'min' => 0, 'required' => true, 'type' => 'number'],
        ['name' => 'start_location_id', 'min' => 0, 'required' => true, 'type' => 'number']];
  }

  public function get($id) {
    /** @var Path $path */
    $path = parent::find($id);
    $path->startLocation;
    $path->endLocation;
    return $path->toArray();
  }

  public function getAll() {
    $paths = parent::all();
    foreach ($paths as $path) {
      /** @var Path $path */
      $path->startLocation;
      $path->endLocation;
    }
    return $paths->toArray();
  }


  /**
   * @return array
   */
  public function rules() {
    return ['end_location_id' => 'required|numeric|min:0', 'start_location_id' => 'required|numeric|min:0'];
  }
}