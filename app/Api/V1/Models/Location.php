<?php

namespace App\Api\V1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model {

  protected $fillable = ['lat', 'lng'];

  public $timestamps = false;

  public function formRules() {
    return [];
  }
}
