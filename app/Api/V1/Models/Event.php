<?php

namespace App\Api\V1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model {

  use SoftDeletes;

  protected $fillable = ['name', 'date', 'description', 'duration', 'location', 'lat', 'lng', 'iso3'];

}
