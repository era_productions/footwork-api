<?php

namespace App\Api\V1\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Path extends Model {

  protected $hidden = ['end_location_id', 'start_location_id'];

  protected $fillable = ['end_location_id', 'start_location_id'];

  public $timestamps = false;

  public function endLocation() {
    return $this->belongsTo('App\\Api\\V1\\Models\\Location', 'end_location_id');
  }

  public function startLocation() {
    return $this->belongsTo('App\\Api\\V1\\Models\\Location', 'start_location_id');
  }

  public function formRules() {
    return [];
  }
}
